import './App.css'
import { useState } from 'react'
import Input from './components/Input'
import Card from './components/Card'

function App() {
  
  const [input, setInput] = useState("")
  const [repoList, setRepoList] = useState([])
  
  const getRepo = () => {
    fetch(`https://api.github.com/repos/${input}`)
    .then(function(response) 
    { if (response.status === 200) {
        response.json().then((response) => setRepoList([...repoList, response]))
      }
      else {window.alert("Erro na busca do repositório")}
    })
    
  }

  return (
    <div className="App">
      <main className="main">
        <section className="inputDiv">
          <Input setInput={setInput} getRepo={getRepo}></Input>
        </section>
        <section>
          <Card repoList={repoList}></Card>
        </section>
      </main>
    </div>
  );
}

export default App;
