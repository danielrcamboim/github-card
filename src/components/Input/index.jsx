
const Input = ({setInput, getRepo}) => {
    
    return(
        <div>
            <input onChange={ (e) => setInput(e.target.value) }/>
            <button onClick={getRepo}>Pesquisar</button>
        </div>
    )
}

export default Input