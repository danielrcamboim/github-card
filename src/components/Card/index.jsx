const Card = ({repoList}) => {
    return(
        <div>
            { repoList.map((item) => {
                return(
                     <a href={item.html_url} target="_blank" rel="noopener noreferrer">
                        <div className="card">
                            <div>
                                <img src={item.owner.avatar_url} alt="Repo Avatar"/>
                            </div>
                            <div className="cardDescription">
                                <h1>{item.full_name}</h1>
                                <p>{item.description}</p>
                            </div>
                        </div>
                    </a>       
                )
            })}
        </div>
    )
}

export default Card